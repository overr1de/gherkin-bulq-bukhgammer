Feature: remove item from watch list

  Background: User logs in to bulq.com
    Given an open browser with bulq.com
    Given I click SignUp/Login button
    Given I log in with Login "bukhgammer@gmail.com" and Password "qwer1234"

  Scenario: remove all items from Watch List
    Given I click at Watch List in the navbar
    When I remove all watched lots
    When I click at Watch List in the navbar
    Then Empty Watch List Text is "Your Watch List is empty right now."