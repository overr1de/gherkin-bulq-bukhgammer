Feature: add items to watch list
  As the registered user
  I want to add lots to watch list
  In order to review it later, remove, or add to cart

  Background: User logs in to bulq.com
    Given an open browser with bulq.com
    Given I click SignUp/Login button
    Given I log in with Login "bukhgammer@gmail.com" and Password "qwer1234"

  Scenario: User adds User & Office equipment to Watch List
    Given I select Category "computers-office"
    When I click Watch at 1st position in the results
    Then Watch List count equals "1"