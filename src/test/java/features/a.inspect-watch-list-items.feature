Feature: Inspect Bulq Watch List Items

  Scenario: guest is required to log in or register to see Watch List

    Given an open browser with bulq.com
    When I click at Watch List in the navbar
    Then url redirects to "https://www.bulq.com/account/sign_in/"
    Then login form is present
    Then sign-up form is present


  Scenario: user can access Watch List from the main page

    Given an open browser with bulq.com
    Given I click at Watch List in the navbar
    Given I log in with Login "bukhgammer@gmail.com" and Password "qwer1234"
    When I click at Watch List in the navbar
    Then Empty Watch List Text is "Your Watch List is empty right now."
