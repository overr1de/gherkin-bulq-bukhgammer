package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.Properties;


@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        monochrome = true,
        features = "src/test/java/features",
        glue = "stepdefinitions",
        plugin = {"pretty", "html:target/cucumber-html-report"})
public class TestCucumberRunner {

    public static Properties config = null;
    public static WebDriver driver = null;


}