package stepdefinitions;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;


public class InspectWatchListItemsStepDefinitions {

    @Before()
    public void beforeScenario() {

        Configuration.startMaximized = false;
        Configuration.reportsFolder = "target/surefire-reports";

        // Making sure we can run tests on win
        String platform;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            platform = "src/test/resources/webdrivers/win/";
        } else if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
            platform = "src/test/resources/webdrivers/osx/";
        } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
            platform = "src/test/resources/webdrivers/linux/";
        } else {
            platform = "src/test/resources/webdrivers/linux/";  // consider the rest nix systems to comply with Linux
        }

        // pass -Dbrowser=chrome or -Dbrowser=firefox in CLI to chose the browser to test in
        String browser = System.getProperty("browser", "chrome").toLowerCase();
        if (browser.equals("chrome")) {
            Configuration.browser = "chrome";
            String chrome_driverPath = new File(platform, "chromedriver").toString();
            System.setProperty("webdriver.chrome.driver", chrome_driverPath);
            System.setProperty("selenide.browser", "Chrome");
        } else if (browser.equals("firefox")) {
            Configuration.browser = "firefox";
            String firefox_driverPath = new File(platform, "geckodriver.exe").toString();
            System.out.print(firefox_driverPath);
            System.setProperty("webdriver.gecko.driver", firefox_driverPath);
            System.setProperty("selenide.browser", "Firefox");
        } else { // by default it's Chrome
            String chrome_driverPath = new File(platform, "chromedriver").toString();
            Configuration.browser = "chrome";
            System.setProperty("webdriver.chrome.driver", chrome_driverPath);
            System.setProperty("selenide.browser", "Chrome");
        }

        open("https://bulq.com");
    }

    public void getUserWithWatchedLots() {
        // Hypothesis-like retrieval, could be implemented later with ORM bindings
        // pseudocode:
        // User user = new User()
        //      .isApproved(true)
        //      .hasWatchedLots(true)
        // String userToken = user.userToken
        //
        // then inject it something like (BULQ has _px2 as user token AFAIK:
        // Cookie ck = new Cookie("_px2", userToken);
        // WebDriverRunner.getWebDriver().manage().addCookie(ck);
    }


    @After()
    public void afterScenario() {
        WebDriverRunner.getWebDriver().quit();
    }

    @Given("^an open browser with bulq.com$")
    public void anOpenBrowserWithBulqCom() {
        open("https://bulq.com");
    }

    @When("^I click at Watch List in the navbar$")
    public void iClickAtWatchListInTheNavbar() {
        $(By.xpath("//div[@class='col-xs-12 dashed-border-bottom']/nav/ul[@class='nav navbar-nav navbar-right']//a[@href='/account/starred/lots/']")).click();
    }

    @Then("^url redirects to \"([^\"]*)\"$")
    public void urlRedirectsTo(String arg0) {
        assert WebDriverRunner.url().equals(arg0);
    }

    @Then("^login form is present$")
    public void loginFormIsPresent() {
        $(By.xpath("//form[@id='new_account']/input[@name='commit']")).shouldBe(visible);
    }

    @Then("^sign-up form is present$")
    public void signUpFormIsPresent() {
        $(By.xpath("//a[@href='/account/sign_up/']")).shouldBe(visible);
    }


    @Given("^I log in with Login \"([^\"]*)\" and Password \"([^\"]*)\"$")
    public void iLogInWithLoginLoginAndPasswordPassword(String arg0, String arg1) {
        $(By.xpath("/html//input[@id='account_email']")).sendKeys(arg0);
        $(By.xpath("/html//input[@id='account_password']")).sendKeys(arg1);
        $(By.xpath("//form[@id='new_account']/input[@name='commit']")).click();
    }

    @Then("^Empty Watch List Text is \"([^\"]*)\"$")
    public void emptyWatchListTextIs(String arg0) {
        $(".info-text").shouldHave(text(arg0));
    }

    @Given("^I click SignUp/Login button$")
    public void iClickSignUpLoginButton() {
        $(By.linkText("SIGN UP / Login")).click();
    }

    @Given("^I select Category \"([^\"]*)\"$")
    public void iSelectCategory(String arg0) {
        $(".header-nav-link--pallet").click();
        String category_drop_down = String.format("//a[@href='/lots/%s/']//div[@class='facet-heading']", arg0);
        $(By.xpath(category_drop_down)).click();
    }

    @When("^I click Watch at (\\d+)st position in the results$")
    public void iClickWatchAtStPositionInTheResults(int arg0) {
        $$(By.xpath("//div[@class='pallet-list']//div[@class='pallet-square__container']"))
                .get(arg0).find(".btn .trigger").click();
    }

    @Then("^Watch List count equals \"([^\"]*)\"$")
    public void watchListCountEquals(String arg0) {
        $(By.xpath("//span[text()='Unwatch']")).shouldBe(visible);
        $(By.xpath("/html//span[@id='watched-lot-count']")).shouldHave(text(arg0));
    }

    @When("^I remove all watched lots$")
    public void iRemoveAllWatchedLots() {
        $(By.xpath("//span[text() ='Unwatch']")).shouldBe(visible); // pre-required assertion before the iteration
        $$(".starred-lots-section--has-items").forEach(item ->
                $(By.xpath("//span[text() ='Unwatch']")).shouldBe(visible).click()
        );
    }
}
