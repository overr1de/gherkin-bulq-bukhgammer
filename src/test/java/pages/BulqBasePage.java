package pages;

/**
 * Base Bulq Page
 * contains base page controls throughout the whole project
 * this class in intended to be inherited from as to pass to every other
 * page object the Pages' footer & header interactions
 */
public class BulqBasePage {
    // In case we move from being functional and KISS with BDD and cucumber
    // And pages are full with loads of logic, step defs can make use of
    // Page Object Patters and implement page functionality here, but
    // I'd rather omit wrapping abstractions into abstractions
}
